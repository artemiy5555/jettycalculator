import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletHandler;
import servlet.CalculatorServlet;
import servlet.HistoryServlet;

import java.util.logging.Logger;

public class Main {

    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {

        int port = 8080;

        Server server = new Server();
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(port);
        server.setConnectors(new Connector[]{connector});

        ServletHandler servletHandler =new ServletHandler();
        server.setHandler(servletHandler);
        servletHandler.addServletWithMapping(CalculatorServlet.class, "/func");
        servletHandler.addServletWithMapping(HistoryServlet.class, "/history");

        try {
            server.start();
            logger.info("Listening port : " + port );
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
