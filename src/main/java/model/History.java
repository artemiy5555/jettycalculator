package model;

public class History {


    private String operation;

    public History(String operation) {
//        this.id = id;
        this.operation = operation;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }



    @Override
    public String toString() {
        return "History{" +
                ", operation='" + operation + '\'' +
                '}';
    }
}
