package servlet;

import model.History;
import service.Calculator;
import service.HistoryService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/func")
public class CalculatorServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Calculator calculator = new Calculator();
        String strOne = request.getParameter("a");
        String strTwo = request.getParameter("b");
        String strOperation = request.getParameter("op");
        double valueOne = 0;
        double valueTwo = 0;
        boolean noError = true;
        try {
            valueOne = Double.parseDouble(strOne);
            valueTwo = Double.parseDouble(strTwo);
        } catch (Exception ex) {
            noError = false;
        }

        if (noError) {

            double result = 0;

            try {

                switch (strOperation) {
                    case "+":
                        result = calculator.functionSum(valueOne, valueTwo);
                        break;
                    case "-":
                        result = calculator.functionDif(valueOne, valueTwo);
                        break;
                    case "*":
                        result = calculator.functionMul(valueOne, valueTwo);
                        break;
                    case "/":
                        if (valueTwo != 0) {
                            result = calculator.functionDiv(valueOne, valueTwo);
                        } else {
                            noError = false;
                        }
                        break;
                    default:
                        noError = false;
                }

            } catch (Exception ex) {
                noError = false;
            }

            if (noError) {
                doSetResult(response, result);
                HistoryService.insert(new History(strOne + strOperation + strTwo + "=" + result));
                return;
            }

        }
        doSetError(response);
    }

    private void doSetResult(HttpServletResponse response, double result) throws IOException {
        String reply = "{\"error\":0,\"result\":" + result + "}";
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
        PrintWriter out = response.getWriter();
        out.print(reply);
        out.flush();
    }

    private void doSetError(HttpServletResponse response) throws IOException {
        String reply = "{\"error\":1}";
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        PrintWriter out = response.getWriter();
        out.print(reply);
        out.flush();
    }


}