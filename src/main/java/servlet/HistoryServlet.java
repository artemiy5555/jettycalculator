package servlet;

import com.google.gson.Gson;
import service.HistoryService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/history")
public class HistoryServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        List histories = HistoryService.selectAlbum();
        String reply = new Gson().toJson(histories);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        PrintWriter out = response.getWriter();
        if(histories!=null) {
            out.print(reply);
            response.setStatus(HttpServletResponse.SC_OK);
        }else{
            out.print(reply);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        out.flush();
    }

}
