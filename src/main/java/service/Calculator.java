package service;

public class Calculator {

    public double functionSum(double a, double b) {
        return a + b;
    }

    public double functionDif(double a, double b) {
        return a - b;
    }

    public double functionMul(double a, double b) {
        return a * b;
    }

    public double functionDiv(double a, double b) {
        return a / b;
    }
    
}
