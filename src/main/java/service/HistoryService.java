package service;

import model.History;
import util.MySqlUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class HistoryService {

    private static Connection c = MySqlUtil.getDBConnection();
    private static final Logger logger = Logger.getLogger(HistoryService.class.getName());

    public static void insert(History history) {

        if (c != null) {
            try {
                PreparedStatement statement = c.prepareStatement(
                        "INSERT INTO calculator.history(operation) VALUES (?)"
                );

                statement.setString(1, history.getOperation());
                statement.execute();
                statement.clearParameters();
                statement.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            logger.warning("Нет конекта");
        }
    }

    public static List selectAlbum() {

        List<History> histories = null;
        if (c != null) {
            histories = new ArrayList<>();
            try {
                Statement statement = c.createStatement();
                ResultSet set = statement.executeQuery(
                        "SELECT * FROM calculator.history"
                );

                while (set.next()) {
                    histories.add(new History(
                            set.getString(2)
                    ));
                }
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else {
            logger.warning("Нет конекта");
        }
        return histories;
    }

}
